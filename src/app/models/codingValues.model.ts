import { Deserializable } from './deserializable.model';



export class CodingValues implements Deserializable {
    allowCoding: boolean;
    allowMultipleValue: boolean;
    codingValues: string;
    delimeter: string;
    allowPredefinedValues: boolean;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}

