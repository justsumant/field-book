import { Deserializable } from './deserializable.model';



export class FieldValues implements Deserializable {
    type: string;
    length: number;
    precision: number;
    scale: number;
    allowEmpty: boolean;
    defaultValue: string;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}

