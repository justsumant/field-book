import { CodingValues } from './codingValues.model';
import { Deserializable } from './deserializable.model';
import { FieldValues } from './fieldValues.model';

export class CustomFieldItem {
    fieldName: string;
    displayName: string;
    notes: string;
    fieldValues: string;
    coding: string;
    makeSearchable: boolean;
}
