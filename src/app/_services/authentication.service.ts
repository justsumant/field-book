﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';
import { User } from '@app/_models';

const users: User[] = [{ id: 1, username: 'satish', password: 'ss', firstName: 'Test', lastName: 'User' }];

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        return this.http.post<any>(`${environment.apiUrl}/users/authenticate`, { username, password })
            .pipe(map(user => {
                // store user details and basic auth credentials in local storage to keep user logged in between page refreshes
                user.authdata = window.btoa(username + ':' + password);
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.removeAccessToken();
        this.currentUserSubject.next(null);
    }

    setApiAccessToken (username: string, password: string) {
        const headersObject = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
        const httpOptions = {
          headers: headersObject
        };
        const data = 'grant_type=password&username=' + encodeURIComponent(username) + '&password=' + encodeURIComponent(password);
        return this.http.post(`${environment.apiUrl}/token`, data, httpOptions )
        .pipe(map(user => {
            // store user details and basic auth credentials in local storage to keep user logged in between page refreshes
                // localStorage.setItem('api_access_token', JSON.stringify(user.access_token));
                this.setAccessToken(user.access_token);
        }));
    }

    getAccessToken() {
        return localStorage.getItem('access_token');
    }
    setAccessToken(token: string) {
        localStorage.setItem('access_token', token);
    }
    removeAccessToken() {
        localStorage.removeItem('access_token');
    }
}
