import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';
import { User } from '@app/_models';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  API_KEY = 'YOUR_API_KEY';
  BASE_URL = 'http://localhost/FieldManagement'
  token: string;
  
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private httpClient: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  private getToken() {
    this.login('satish', 'ss').subscribe((data) => {
      console.log(data);
    });
  }

 private login(username: string, password: string) {
    const headersObject = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    const httpOptions = {
      headers: headersObject
    };
    const data = 'grant_type=password&username=' + encodeURIComponent(username) + '&password=' + encodeURIComponent(password);
    return this.httpClient.post(`${environment.apiUrl}/token`, data, httpOptions );
  }

  public getProjects() {
    // tslint:disable-next-line: max-line-length
    const t = `h9giQtl7BtcVlkObux3B8ZovLOJCfzwFHWUoDUcj1NoFPUYa9eFJ4aYux5relV3CcjZDSi8x7V53n0DNuPwy13G97sA2hDpoUr7AFjdd7MR6BF_OVkm7PJ3eeKHX0LB7UKykaVnZJmPeavIEOth-_f73_Esp477W7hNfJOQMBZ9uAIVI-oNW8rT4Xx1pLc-_-MW1WN9glidKRhHdu62jRaXTdXhp9NjTjFcXJxS-dPDma1_9o_m4fUtGPll5PvLXrzVWnCdkEsJ-loAn_3_60g`;
    const headersObject = new HttpHeaders().set('Authorization', 'Bearer ' + t);
    const httpOptions = {
          headers: headersObject
        };
    return this.httpClient.get('http://localhost/FieldManagement/vod/services/cases', httpOptions);
  }
}
