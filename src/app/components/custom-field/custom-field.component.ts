import { Component, OnInit } from '@angular/core';
import { CustomFieldItem } from 'src/app/models/customFieldItem.model';
import { FieldValues } from '@app/models/fieldValues.model';

@Component({
  selector: 'app-custom-field',
  templateUrl: './custom-field.component.html',
  styleUrls: ['./custom-field.component.css']
})
export class CustomFieldComponent implements OnInit {

  customFieldItem: CustomFieldItem;

  customFieldList: Array<CustomFieldItem> = [];

  constructor() { }

  ngOnInit() {
    this.customFieldItem = {
      fieldName: '',
      displayName: '',
      notes: '',
      fieldValues: '',
      coding: '',
      makeSearchable: true
    }
  }


  addCustomField() {
    console.log(this.customFieldItem);
    this.customFieldList.push(this.customFieldItem);
    // this.customFieldItem = {};f
  }

}
