import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api/api.service';

@Component({
  selector: 'app-field-management',
  templateUrl: './field-management.component.html',
  styleUrls: ['./field-management.component.css']
})
export class FieldManagementComponent implements OnInit {
  public caseList: any;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getProjects().subscribe((data) => {
      this.caseList = data;
      console.log(data);
    });
  }

  onProjectChange(projectId: any) {
    console.log(projectId)
    if (projectId === '0') {
      console.log(1);
    } else {
    }
}

}
